﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;

using System.Web.OData;

using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using WebApplication3_api.Models;
using System.Web.Http.OData.Formatter;

namespace WebApplication3_api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Конфигурация и службы Web API
            // Настройка Web API для использования только проверки подлинности посредством маркера-носителя.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Маршруты Web API
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );




            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<Product>("Products");
            config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());

            ODataConventionModelBuilder builder3 = new ODataConventionModelBuilder();
            builder3.EntitySet<Product>("Products3");
            config.Routes.MapODataServiceRoute("odata3", "odata", builder3.GetEdmModel());
            //config.EnableQuerySupport();

            //var formatters = ODataMediaTypeFormatters.Create();
            //config.Formatters.InsertRange(0, formatters);

            //ODataModelBuilder builder = new ODataModelBuilder();
            //builder.EntitySet<Product>("Products");
            //var model = builder.;

            //config.MapODataServiceRoute(routeName: "ODataRoute", routePrefix: null, model: model);
        }
    }
}

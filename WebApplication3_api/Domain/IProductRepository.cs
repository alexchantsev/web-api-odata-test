﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using WebApplication3_api.Models;

namespace WebApplication3_api.Domain
{
    public interface IProductRepository
    {
        Product Get(int id);
        IEnumerable<Product> FindAll();
        IEnumerable<Product> Find(Expression<Func<Product, bool>> expression);
    }
}
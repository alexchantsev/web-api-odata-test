﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using WebApplication3_api.Models;

namespace WebApplication3_api.Domain
{
    public class TestProductRepository : IProductRepository
    {
        private List<Product> products;
        public TestProductRepository()
        {
            products = new List<Product>();
            products.Add(new Product() { Id = 25, Name = "Телевизор" });
            products.Add(new Product() { Id = 1, Name = "Пылесос" });
            products.Add(new Product() { Id = 2, Name = "Самовар" });
            products.Add(new Product() { Id = 3, Name = "Холодильник" });
            products.Add(new Product() { Id = 4, Name = "Лампа настольная" });
            products.Add(new Product() { Id = 5, Name = "Монитор" });
            products.Add(new Product() { Id = 6, Name = "Телефон" });
            products.Add(new Product() { Id = 7, Name = "Автомобиль" });
            products.Add(new Product() { Id = 8, Name = "Диван" });
        }
        public Product Get(int id)
        {
            return products.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Product> FindAll()
        {
            return products;
        }

        public IEnumerable<Product> Find(Expression<Func<Product, bool>> expression)
        {
            
            return products.Where(expression.Compile());
        }
    }
}
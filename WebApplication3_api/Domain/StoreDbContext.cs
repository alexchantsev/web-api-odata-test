﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApplication3_api.Models;

namespace WebApplication3_api.Domain
{
    public class StoreDbContext: DbContext
    {
        public StoreDbContext()
            : base("name=db") { }

        public DbSet<Product> Products { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using WebApplication3_api.Domain;
using WebApplication3_api.Models;

namespace WebApplication3_api.Controllers
{
    
    public class ProductsController : ODataController
    {
        //private static ODataValidationSettings _validationSettings = new ODataValidationSettings();
        
        IProductRepository _repository;
        public ProductsController()
        {
            _repository = new TestProductRepository();
        }

        [HttpGet]
        [EnableQuery]
        public Product Get(int id)
        {
            var product = _repository.Get(id);
            if (product == null)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Не найден"));
            }
            else
            {
                return product;
            }
        }

        [HttpGet]
        [EnableQuery]
        public IQueryable<Product> GetProducts()
        {
            return _repository.FindAll().AsQueryable();
        }
    }
}
